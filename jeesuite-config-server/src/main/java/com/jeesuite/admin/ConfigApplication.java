package com.jeesuite.admin;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jeesuite.security.SecurityDelegatingFilter;
import com.jeesuite.springboot.starter.BaseApplicationStarter;

@Controller
@SpringBootApplication
@ComponentScan(value = { "com.jeesuite.admin","com.jeesuite.springboot.autoconfigure" })
@EnableTransactionManagement
public class ConfigApplication extends BaseApplicationStarter{
	public static void main(String[] args) {

		long start = before();
		new SpringApplicationBuilder(ConfigApplication.class).web(WebApplicationType.SERVLET).run(args);
		after(start);
	}
	
	@Bean
	public FilterRegistrationBean<SecurityDelegatingFilter> someFilterRegistration() {
	    FilterRegistrationBean<SecurityDelegatingFilter> registration = new FilterRegistrationBean<>();
	    registration.setFilter(new SecurityDelegatingFilter());
	    registration.addUrlPatterns("/user/*");
	    registration.addUrlPatterns("/admin/*");
	    registration.setName("authFilter");
	    registration.setOrder(0);
	    return registration;
	}

	@RequestMapping("/")
	String home() {
		return "redirect:/admin.html";
	}

}
